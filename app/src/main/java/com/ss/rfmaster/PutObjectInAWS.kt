package com.ss.rfmaster

import android.graphics.Region
import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import aws.smithy.kotlin.runtime.content.asByteStream
import java.nio.file.Paths

import aws.sdk.kotlin.services.s3.S3Client
import aws.sdk.kotlin.services.s3.model.*
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3Client
import java.io.IOException
import com.amazonaws.regions.Regions
import java.io.BufferedReader
import java.io.File


class PutObjectInAWS {

    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun main(args: Array<String>) {

        val usage = """
    Usage:
        <bucketName> <objectKey> <objectPath>
    Where:
        bucketName - The Amazon S3 bucket to upload an object into.
        objectKey - The object to upload (for example, book.pdf).
        objectPath - The path where the file is located (for example, C:/AWS/book2.pdf).
    """
        val KEY = "AKIAZJHZCSRA6HTADRFT"
        val SECRET = "IwfRrKSddD3teuDHVHdkcBwzK5HUubF6Yvo4/zv1"

        var s3Client: AmazonS3Client? = null
        var credentials: BasicAWSCredentials? = null

        credentials = BasicAWSCredentials(KEY, SECRET)

        val clientRegion = Regions.AP_NORTHEAST_2

        s3Client = AmazonS3Client(credentials)
        s3Client.setRegion(com.amazonaws.regions.Region.getRegion(Regions.AP_SOUTHEAST_2))

        val bucketName = args[0]
        val objectKey = args[1]
        val objectPath = args[2]


        val metadataVal = mutableMapOf<String, String>()
        metadataVal["myVal"] = "test"
        val f = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "RFLogAWS$.txt")
        var fileExists = f.exists()
        if (fileExists) {
            val bufferedReader: BufferedReader = File(
                Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOWNLOADS
                ), "RFLog2022-07-01-16-00.txt"
            ).bufferedReader()
            val inputString = bufferedReader.use { it.readText() }

            try {
                val request = PutObjectRequest {
                    bucket = "dev.datalake.aws.servicestream.com.au"
                    key = objectKey
                    metadata = metadataVal
                    this.body = Paths.get(objectPath).asByteStream()
                    val response = s3Client.putObject(
                        "dev.datalake.aws.servicestream.com.au/RF_logs_SEW",
                        "RFLog2022-07-01-16-00.txt",
                        inputString
                    )
                    print(response)
                }
            } catch (e: IOException) {
                println("ERROR (S3Exception): " + e)
            }
        } else {
            println("File doesn't exist or program doesn't have access to it")
        }


    }


    @RequiresApi(Build.VERSION_CODES.O)
    suspend fun putS3Object(
        s3Client: S3Client,
        bucketName: String,
        objectKey: String,
        objectPath: String
    ) {
        try {
            val metadataVal = mutableMapOf<String, String>()
            metadataVal["myVal"] = "test"

            val putOb = PutObjectRequest {
                bucket = bucketName
                key = objectKey
                metadata = metadataVal
                this.body = Paths.get(objectPath).asByteStream()
            }

            val response = s3Client.putObject(putOb)
            println("Tag information is ${response.eTag}")

        } catch (e: S3Exception) {
            println(e.message)
            s3Client.close()
//            exitProcess(0)
        }
    }


}