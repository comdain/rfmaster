package com.ss.rfmaster

import org.java_websocket.WebSocket
import java.nio.ByteBuffer

interface WsServerListener {
    fun onWsServerStatusChanged(isRunning: Boolean)

    fun onWsServerError(errorType: Int, errorMessage: String?)

    fun onWsServerConnChanged(connList: MutableList<String>?)

    fun onMessage(conn: WebSocket?, message: String?)

    fun onMessage(conn: WebSocket?, message: ByteBuffer?)

    fun onWsServerMessageToClientSent(message: String)

    fun onWsNoConnections(message: String)

    fun onWsClientConnectionClosed()
}
