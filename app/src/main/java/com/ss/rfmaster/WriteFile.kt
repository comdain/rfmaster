package com.ss.rfmaster

import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import okhttp3.Response
import okio.Okio
import java.io.File
import java.nio.file.Files

class WriteFile {

    @RequiresApi(Build.VERSION_CODES.O)
    fun writeFile(response: Response, result: (result: String) -> Unit){
        var dirPAth = File(Environment.getExternalStoragePublicDirectory("Itron Driver Service").toString())
        val file: File = File(Environment.getExternalStoragePublicDirectory("Itron Driver Service/ItronLicense.lic").toString())
        if (dirPAth.exists() && dirPAth.isDirectory) {
            val sink = Okio.buffer(Okio.sink(file))
            sink.writeAll(response.body()!!.source())
            sink.close()
            result.invoke("Success")
        }
        else
        {
            result.invoke("Directory not found")
        }

    }
}