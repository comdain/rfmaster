package com.ss.rfmaster

import java.io.IOException
import java.nio.charset.Charset
import android.app.PendingIntent.getActivity
import okhttp3.*
import org.json.JSONException

/**
 * Created by Rohan Jahagirdar on 07-02-2018.
 */


import java.util.HashMap

import java.util.Map

class OkHttpRequest(client: OkHttpClient) {
    internal var client = OkHttpClient()

    init {
        this.client = client
    }

    companion object {
        val JSON = MediaType.parse("application/json; charset=utf-8")
    }

}