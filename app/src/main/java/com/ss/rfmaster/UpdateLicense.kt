package com.ss.rfmaster

import android.os.Environment
import java.io.File

class UpdateLicense {

    fun addLicenseInDevice()
    {
        val from = File(Environment.getExternalStoragePublicDirectory("Itron Driver Service").toString() + "/Log/2022-02-25_Response.log")
        val to = File(Environment.getExternalStoragePublicDirectory("Itron Driver Service").toString() + "/DeviceLicenseNumber11.lic")
        from.copyTo(to)
    }

}