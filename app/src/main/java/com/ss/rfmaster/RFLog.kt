package com.ss.rfmaster

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import android.os.Environment
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import java.io.File
import java.time.LocalDateTime
import java.util.*
import androidx.core.content.ContextCompat.getSystemService

import android.telephony.TelephonyManager
import androidx.core.content.ContextCompat
import android.bluetooth.BluetoothAdapter
import android.provider.Settings
import java.security.AccessController.getContext


class RfLogs {
    var okHTTPClient = RESTClient()
    @RequiresApi(Build.VERSION_CODES.O)
    fun appendLog(text: String?, context: Context, fileEndPoint: String) {
        val cal = Calendar.getInstance()
        val resultdate = Date(cal.getTimeInMillis())
        val sdf = SimpleDateFormat("YYY-MM-dd HH")
        var dateString = sdf.format(resultdate).replace(" ","-")
        dateString = dateString + "-00"
        val activity = context as Activity
        val REQUEST_EXTERNAL_STORAGE = 1
        val PERMISSIONS_STORAGE = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        val permission: Int = ActivityCompat.checkSelfPermission(context,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        // val f = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), "RFLog$dateString.txt")
        val sb = StringBuilder()
        val now = LocalDateTime.now()
        var nowTime = now.toLocalTime()
        sb.append(nowTime).append("," + text)
        // sb.append("\n")
        var c = sb.toString()
        c = c.padEnd(0)
        if(ActivityCompat.checkSelfPermission(context,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED) {
            try {

                val deviceId = Settings.Secure.getString(
                    context.getContentResolver(), Settings.Secure.ANDROID_ID
                )
                val dir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    "$fileEndPoint")

                  if (!dir.exists()){
                      dir.mkdir()
                  }

                val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    "$fileEndPoint/$deviceId-$fileEndPoint$dateString.txt")
                var fileExists = file.exists()



                if(fileExists){
                    if (fileEndPoint == "AWS" || fileEndPoint == "SPLogs")
                    {
                        file.appendText(",$text")
                    }
                    else{
                        file.appendText("$c")
                    }
                    file.readLines().forEach { line -> Log.e("LOG", line)}
                } else {
                    print(file.name)

                    if(fileEndPoint == "AWS"|| fileEndPoint == "SPLogs"){
                        file.appendText("[\n$text")
                    }
                    else{
                        file.appendText("$c")
                    }

                    file.readLines().forEach { line -> Log.e("LOG", line)}
                }
            }
            catch (ex: Exception)
            {
                print(ex)
            }
        }
        else{
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
        }
    }

    fun checkFiles()
    {
        val files: Array<File> = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "SPLogs").listFiles()
        if (files != null && files.size > 0) for (file in files) {
                uploadLogFile(file.name)
        }
    }

    fun uploadLogFile(fileName :String) {
        var file: File = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "SPLogs/$fileName"
        )
        okHTTPClient.getAccessToken(){result ->
            if (result !== "")
            {
                okHTTPClient.uploadFile(result,file) { response ->
                    if(response == "Success")
                    {
                        print("Successfulyl uploaded file to Sharepoint")

                    }
                    else
                    {
//                        showAlert("Directory not found")
                    }
                }

            }
        }

    }

}