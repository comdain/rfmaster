package com.ss.rfmaster;

import android.util.Log;

import org.java_websocket.WebSocket;
import org.java_websocket.exceptions.WebsocketNotConnectedException;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class WsServer extends WebSocketServer {

    private static final String TAG = "WsServer";

    public static final int ERROR_TYPE_NORMAL = 0;
    public static final int ERROR_TYPE_PORT_IN_USE = 1;
    public static final int ERROR_TYPE_SERVER_CLOSE_FAIL = 2;
    public static final int ERROR_TYPE_SOCKET_NOT_OPEN = 3;

    private WsServerListener wsServerListener;

    List<WebSocket> webSockets = new ArrayList<>();

    public void setListener(WsServerListener listener) {
        wsServerListener = listener;
    }

    public WsServer(InetSocketAddress address) {
        super(address);
    }

    public static WsServer init(String host, int port) {
        return new WsServer(new InetSocketAddress(host, port));
    }

    public void stopWithException() {
        try {
            this.stop();
        } catch (IOException e) {
            e.printStackTrace();
            wsServerListener.onWsServerError(ERROR_TYPE_SERVER_CLOSE_FAIL, e.getMessage());//关闭服务失败
        } catch (InterruptedException e) {
            e.printStackTrace();
            wsServerListener.onWsServerError(ERROR_TYPE_SERVER_CLOSE_FAIL, e.getMessage());//关闭服务失败
        }
    }

    public void closeAllClients() {
        for (WebSocket conn: webSockets) {
            conn.close();
        }
        }


    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        String connIp = conn.getRemoteSocketAddress().getAddress().toString().replace("/", "");
        connList.add(connIp);
        webSockets.add(conn);
        wsServerListener.onWsServerConnChanged(connList);
        Log.d(TAG, "onOpen: // " + connIp + " //Opened connection number  " + connList.size());

      //  conn.send("Connection request received by server! Welcome to the server!");
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        Log.d(TAG, "onClose: ");
        wsServerListener.onWsClientConnectionClosed();
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        Log.d(TAG, "onMessage: " + message);
        wsServerListener.onMessage(conn, message);
    }

    @Override
    public void onMessage(WebSocket conn, ByteBuffer message) {
        Log.d(TAG, "onMessage: buffer");
        wsServerListener.onMessage(conn, message);
    }

    private boolean running = false;

    public boolean isRunning() {
        return running;
    }

    private List<String> connList = new ArrayList<>();

    @Override
    public void onError(WebSocket conn, Exception ex) {
        Log.d(TAG, "onError: " + ex.getMessage());
        ex.printStackTrace();
        if (ex.getMessage() != null) {
            if (ex.getMessage().contains("Address already in use")) {
                Log.d(TAG, "ws server: 端口已被占用");
                wsServerListener.onWsServerError(
                        ERROR_TYPE_PORT_IN_USE,
                        "Address already in use"
                );//服务启动失败，端口已被占用，请更换端口
                return;
            }
        }
        if(!ex.getMessage().contains("Only the original thread that created a view hierarchy can touch its views"))
        {
            wsServerListener.onWsServerError(ERROR_TYPE_NORMAL, ex.toString());
        }

    }

    @Override
    public void onClosing(WebSocket conn, int code, String reason, boolean remote) {
        super.onClosing(conn, code, reason, remote);
        String connIp = conn.getRemoteSocketAddress().getAddress().toString().replace("/", "");
        for (String ip : connList) {
            if (ip.equals(connIp)) {
                connList.remove(ip);
                break;
            }
        }
        wsServerListener.onWsServerConnChanged(connList);
        Log.d(TAG, "onClosing: // " + connIp + " //Opened connection number  " + connList.size());
    }

    @Override
    public void onStart() {
        running = true;
        wsServerListener.onWsServerStatusChanged(running);//服务启动成功
        Log.d(TAG, "onStart: ");
    }

    protected void sendMessageToClient(String message) {
        try {
            if (hasClients()) {
                broadcast(message);
                wsServerListener.onWsServerMessageToClientSent(message);
            } else {
                wsServerListener.onWsNoConnections("There was no active connection associated with the server, message not sent! Please connect the client to the server");
            }
        } catch (WebsocketNotConnectedException e) {
            wsServerListener.onWsServerError(ERROR_TYPE_SOCKET_NOT_OPEN ,e.getMessage());
        }
    }

    protected boolean hasClients() {
        return !getConnections().isEmpty() || getConnections().size() > 0;
    }
}
