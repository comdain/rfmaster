package com.ss.rfmaster;

import android.app.Service
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.os.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import java.io.FileDescriptor
import java.util.*


class Services : Service() {

    private var serviceLooper: Looper? = null
    private var serviceHandler: ServiceHandler? = null

    inner class MyBinder : Binder() {
        fun getService(): Services? {
            return this@Services
        }
    }

    private inner class ServiceHandler(looper: Looper) : Handler(looper) {

        override fun handleMessage(msg: Message) {
            // Normally we would do some work here, like download a file.
            // For our sample, we just sleep for 5 seconds.
            try {
                Thread.sleep(5000)
            } catch (e: InterruptedException) {
                // Restore interrupt status.
                Thread.currentThread().interrupt()
            }

            // Stop the service using the startId, so that we don't stop
            // the service in the middle of handling another job
            stopSelf(msg.arg1)
        }
    }

    override fun onBind(intent: Intent?): IBinder? {
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onCreate() {
//        Toast.makeText(this, "Service Created",
//            Toast.LENGTH_LONG).show()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(Intent(this, Services::class.java));
        }
        else {
            startService(Intent(this, Services::class.java));
        }

    }

//    override fun onStart(intent: Intent?, startId: Int) {
//       // Toast.makeText(this, "Service Started",
//         //   Toast.LENGTH_LONG).show()
//        // myPlayer.start()
//
//    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        serviceHandler?.obtainMessage()?.also { msg ->
            msg.arg1 = startId
            serviceHandler?.sendMessage(msg)
        }

        return START_STICKY
    }

    override fun onDestroy() {
        Toast.makeText(this, "Service Stopped",
            Toast.LENGTH_LONG).show()


    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun getCurrentTime(): String {
        val dateformat = SimpleDateFormat("HH:mm:ss MM/dd/yyyy",
            Locale.US)
        return dateformat.format(Date())
    }
}