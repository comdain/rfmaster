package com.ss.rfmaster

import android.app.AlertDialog
import android.content.Intent
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import androidx.annotation.RequiresApi
import com.itron.wh.androiddriver.service.aidl.IItronServiceApi
import com.itron.wh.androiddriver.service.aidl.IItronServiceCallback
import org.json.JSONArray
import org.json.JSONObject
import java.time.LocalDate
import java.util.*
import kotlin.collections.HashMap


class RFMasterConnection(val sendResponse: (String,Map<String, Any>, String, String) -> Unit = { s: String,s1: Map<String,Any>,s2: String, s3: String -> },val sendErrorResponse: (String,String, String) -> Unit = { s: String, s2: String , s3: String -> }, val setConnectionstatus: (String) -> Unit = { s: String -> }, val readCybles: () -> Unit = { }, val sendBluetoothErrorResponse: (String, String)-> Unit = { s: String, s1: String -> }){

    var commCommandsObj = CommunicationCommands()
    lateinit var myAidl: IItronServiceApi

    private var cybleNumber: String = ""
    private lateinit var cybleNumbers: Array<String>
    private lateinit var requestedMeters: JSONArray

    private lateinit var wsServer: WsServer
    private var it: Intent? = null

    @RequiresApi(Build.VERSION_CODES.O)
    val current = LocalDate.now()

    @RequiresApi(Build.VERSION_CODES.N)
    val serverFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    @RequiresApi(Build.VERSION_CODES.N)


    var myAidlCallBackForConnectionCheck: IItronServiceCallback = object : IItronServiceCallback.Stub() {
        override fun onStatusUpdated(message: String) {
            val obj = JSONObject(message)
            val result: String
            result = obj.getString("Success")

            if (result !== "") {
                var data: String
                val dataResponseArray: JSONArray                                                                       
                var macAddress: String = ""
                val responseObj = JSONObject(result)
                val command = responseObj.getString("Command")
                if (command == "GetAllConnections") {
                    data = responseObj.getString("Data")
                    val dataObj = JSONObject(data)

                    val connections: JSONArray
                    connections = dataObj.getJSONArray("Connections")
                    if (connections.length() !== 0) {
                        setConnectionstatus("RFMaster Status: Connected")
                    }
                    else
                    {
                        setConnectionstatus("RFMaster Status: Not Connected")
                        getBlutoothDevices()
                    }
                    }
                }
            }
        }


    var myAidlCallBackForConnections: IItronServiceCallback = object : IItronServiceCallback.Stub() {
    override fun onStatusUpdated(message: String) {
        val obj = JSONObject(message)
        val result: String
        result = obj.getString("Success")

        if (result !== "") {
            var data: String
            val dataResponseArray: JSONArray
            var macAddress: String = ""
            val responseObj = JSONObject(result)
            val command = responseObj.getString("Command")
            if (command == "GetAllConnections") {
                data = responseObj.getString("Data")
                val dataObj = JSONObject(data)

                val connections: JSONArray
                connections = dataObj.getJSONArray("Connections")
                if (connections.length() !== 0) {
                    try {
                      //  setConnectionstatus("RFMaster Status: Connected")
                        readCybles()
                    } catch (e: Exception) {
                        wsServer.sendMessageToClient(e.localizedMessage)
                    }

                }
                else
                {
                    setConnectionstatus("RFMaster Status: Not Connected")
                    getBlutoothDevices()
                }
            }
        }
    }
        }

    var myAidlCallBackToGetBluetoothDevices: IItronServiceCallback = object : IItronServiceCallback.Stub()
        {
            override fun onStatusUpdated(message: String) {
                val obj = JSONObject(message)
                val result: String

                result = obj.getString("Success")
                if (result !== "") {
                    var data: String
                    val responseObj = JSONObject(result)
                    val command = responseObj.getString("Command")

                    if (command == "GetBluetoothDevices") {
                        data = responseObj.getString("Data")
                        val dataObj = JSONObject(data)
                        connectRFMasterBluetooth(dataObj)
                    }
                }
            }
        }

    var myAidlCallBackToConnectBluetooth: IItronServiceCallback = object : IItronServiceCallback.Stub()
        {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onStatusUpdated(message: String) {
                print(message)
                val obj = JSONObject(message)

                var result: String = ""

                try {
                    result = obj.getString("Success")
                    if (result !== "") {
                        var data: String
                        val dataResponseArray: JSONArray
                        var macAddress: String = ""
                        val responseObj = JSONObject(result)
                        val command = responseObj.getString("Command")

                        if (command == "OpenBluetooth") {
                            readCybles()
                        }
                    }
                } catch (e: java.lang.Exception) {
                    result = obj.getString("Error")
                    val responseObj = JSONObject(result)
                    val errorCode = responseObj.getString("ErrorCode")
                    val errorMessage = responseObj.getString("ErrorMessage")
                    sendBluetoothErrorResponse(errorMessage, errorCode)
                }
            }
        }

    var myAidlCallBackForCybleReading: IItronServiceCallback = object : IItronServiceCallback.Stub()
        {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onStatusUpdated(message: String) {

                if(message == "")
                {
                    print("empty")
                }
                val obj = JSONObject(message)
                var result: String
                try{
                    result = obj.getString("Data")
                    if (result !== "") {
                        val responseObj = JSONObject(result)
                        val command = responseObj.getString("Data")
                        val legacyData = JSONObject(command).getString("LegacyData")
                        val alarmsArray = JSONObject(legacyData).get("Alarms").toString()
                        var alarms = alarmsArray.substring(1, alarmsArray.length-1);
                        val miuindex = JSONObject(legacyData).get("MiuIndex").toString()

                        val backflow:JSONObject = JSONObject(legacyData).getJSONObject("Backflow")
                        val backflowIndex = backflow.get("Index")

                        //val index = JSONObject(backflow).get("Index")

                        val pulseWeight = JSONObject(legacyData).get("PulseWeight").toString()
                        val serialNumber = JSONObject(command).getString("SerialNumber")
                        val muiIndex = JSONObject(legacyData).getString("MiuIndex")
                        val readDateTime = JSONObject(legacyData).getString("MiuDate")
                        val muiValue = JSONObject(muiIndex).getString("Value")
                        val muiUnit = JSONObject(muiIndex).getString("Unit")

                        val map: HashMap<String, Any> = hashMapOf("alarms" to alarms, "backflowIndex" to backflowIndex, "muiUnit" to muiUnit, "pulseWeight" to pulseWeight)

                        sendResponse(muiValue, map, serialNumber, readDateTime)
                    }
                }catch(e: Exception){

                    print(e.localizedMessage)

                    checkError(obj)
                }
            }
        }


    var myAidlCallBackForLicenseUpdate: IItronServiceCallback = object : IItronServiceCallback.Stub(){
        @RequiresApi(Build.VERSION_CODES.O)
        override fun onStatusUpdated(message: String?) {
            if(message == "")
            {
                print("empty")
            }
            val obj = JSONObject(message)
            var result: String = ""
            try {
                result = obj.getString("Success")
                if (result !== "") {
                    sendResponse("License has been updated Successfully", mapOf(), "" , "")
                }
            }
            catch (e: Exception)
            {
                print(e.localizedMessage)
                result = obj.getString("Error")
                if(JSONObject(result).getString("ErrorMessage") !== "")
                {
                    val error = JSONObject(result).getString("ErrorMessage")
                    if(error != "Command started")
                    {
                        sendErrorResponse(error,"","")
                    }

                }
              //  sendErrorResponse("error","","error")
               // val alertDialog = AlertDialog.Builder(this)
            }
            print(message)
        }

    }

        //checkBluetoothConenction()
//    }


    fun getBlutoothDevices()
    {
        val req = commCommandsObj.getBluetoothDevices()
        myAidl.send("com.example.rfmaster", req.toString(), myAidlCallBackToGetBluetoothDevices)
    }

    fun connectRFMasterBluetooth(dataObj: JSONObject)
    {
        val dataResponseArray: JSONArray
        var macAddress: String = ""

        dataResponseArray = dataObj.getJSONArray("Devices")
        val devices = dataResponseArray.getJSONObject(0)
        macAddress = devices.get("MacAddress") as String

        val req = commCommandsObj.connectToBluetoothDevice(macAddress)

        try {
            myAidl.send("com.example.rfmaster", req.toString(), myAidlCallBackToConnectBluetooth)
        }
        catch (e:java.lang.Exception)
        {
            print(e)
        }

    }



    fun checkBluetoothConenction()
    {
        val allConnections = commCommandsObj.getAllConnections()
        myAidl.send("com.example.rfmaster", allConnections.toString(),myAidlCallBackForConnections)
    }

    fun append(element: String, list: MutableList<String>): Array<String> {
        list.add(element)
        return list.toTypedArray()
    }

    fun sendMessageToClient(responseArray: Array<String>)
    {
        wsServer.sendMessageToClient("{\n" +
                "\"msgType\": \"readingResponse\",\n" +
                "\"meterType\": \"remote\",\n" +
                "\"numMeters\": ${responseArray.size.toInt()},\n" +
                "\"meters\": ${Arrays.toString(responseArray)} \n" +
                "}")
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun checkError(obj: JSONObject)
    {
        var result: String = ""
        try{
            result = obj.getString("Information")
            if(JSONObject(result).getString("Message") !== "")
            {
                val error = JSONObject(result).getString("Message")
                if(error != "Command started")
                {
                    val serialNumber = error.filter { it.isDigit() }
                    sendErrorResponse(error,"",serialNumber)
                }

            }
        }

        catch (e:java.lang.Exception)
        {

            result = obj.getString("Error")
            if(JSONObject(result).getString("ErrorMessage") !== "")
            {
                val error = JSONObject(result).getString("ErrorMessage")
                val errorCode = JSONObject(result).getString("ErrorCode")
                if(error != "Command started")
                {
                    val serialNumber = error.filter { it.isDigit() }
                    sendErrorResponse(error,errorCode,"")
                }

            }
        }
    }
}
