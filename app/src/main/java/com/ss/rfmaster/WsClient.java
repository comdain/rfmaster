package com.ss.rfmaster;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.util.Map;

public class WsClient extends WebSocketClient {

    private WsClientListner wsClientListner;

    public void setListener(WsClientListner listener) {
        wsClientListner = listener;
    }

    public WsClient(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public WsClient(URI serverURI) {
        super(serverURI);
    }

    public WsClient(URI serverUri, Map<String, String> httpHeaders) {
        super(serverUri, httpHeaders);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
       // send("Message from WS Client : ServerHandshake ");
    }

    @Override
    public void onMessage(String message) {
        wsClientListner.onClientMessage(message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        // The codecodes are documented in class org.java_websocket.framing.CloseFrame
        System.out.println(
                "Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
                        + reason);
    }

    @Override
    public void onError(Exception ex) {
        ex.printStackTrace();
        // if the error is fatal then onClose will be called additionally
        Log.e("dhara","exception : " + ex.getMessage());
    }
}
