package com.ss.rfmaster

import java.util.*

class CommunicationCommands {

    public fun getBluetoothDevices(): String{
        return """{
                    "Request" : {
                    "RequestUserId" : "1", 
                    "Driver" : "ItronWHDriverCommon",
                    "Command" : "GetBluetoothDevices",
                    "ConnectionId" : null,
                    "Guid": "d2c23ddd-f8b8-45a3-acab-965cabab3a63",
                    "Parameters" : {		
                    }
                    }
                    }"""

    }


    public fun connectToBluetoothDevice(macAddress : String): String {
        return """{"Request" : {
                            "RequestUserId" : "1", 
                            "Driver" : "ItronWHDriverCommon",
                            "Command" : "OpenBluetooth",
                            "ConnectionId" : null,
                            "Guid": "d2c23ddd-f8b8-45a3-acab-965cabab3a63",
                            "Parameters" : {
                                "MacAddress" : "$macAddress"		
                        }
                        }
                        }"""
    }

    public fun readCyble(SerialNumber: String): String {
        return """{"Request" : {
                            "RequestUserId" : "1", 
                            "Driver" : "ItronWHDriverCyble",
                            "Command" : "ReadCyble",
                            "ConnectionId" : 1,
                            "Guid": "d2c23ddd-f8b8-45a3-acab-965cabab3a63",
                            "Parameters" : {
                               "SerialNumber": "$SerialNumber"
                        }
                        }
                        }"""
    }

    public fun readCybles(SerialNumbers: Array<String> ): String {
        return """{"Request" : {
                            "RequestUserId" : "1", 
                            "Driver" : "ItronWHDriverCyble",
                            "Command" : "ReadPollingCyble",
                            "ConnectionId" : 1,
                            "Guid": "d2c23ddd-f8b8-45a3-acab-965cabab3a63",
                            "Parameters" : {
                               "SerialNumbers": ${Arrays.toString(SerialNumbers)}
                        }
                        }
                        }"""
    }

    public fun getAllConnections(): String {
        return """{
                    "Request" : {
                        "RequestUserId" : "1", 
                        "Driver" : "ItronWHDriverCommon",
                        "Command" : "GetAllConnections",
                        "ConnectionId" : null,
                        "Guid": "d2c23ddd-f8b8-45a3-acab-965cabab3a63",
                        "Parameters" : {		
                            }
                        }
                    }"""
    }

    public fun updatelicense():String{
        return """
            {
            "Request": {
                "RequestUserId": 1,
                "Driver": "ItronWHDriverCommon",
                "Command": "UpdateLicense",
                "ConnectionId": null,
                "Guid": "2c23ddd-f8b8-45a3-acab-965cabab3a63",
                "Parameters": {
                    "LicenseFileName": "/storage/emulated/0/Itron Driver Service/ItronLicense.lic"
                }
            }
        }"""
    }

}

