package com.ss.rfmaster

import android.app.Service
import android.content.Intent

import android.os.IBinder
import android.util.Log


class ForegroundService : Service() {
    override fun onCreate() {
        super.onCreate()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        // Your logical code here
        return START_STICKY
    }

    override fun onTaskRemoved(rootIntent: Intent?) {

        //When remove app from background then start it again
        startService(Intent(this, ForegroundService::class.java))
        super.onTaskRemoved(rootIntent)
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(LOG_TAG, "In onDestroy")
    }

    override fun onBind(intent: Intent?): IBinder? {
        // Used only in case of bound services.
        return null
    }

    companion object {
        private const val LOG_TAG = "ForegroundService"
    }
}