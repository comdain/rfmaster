package com.ss.rfmaster

import android.Manifest.permission.*
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.*
import android.os.Build.VERSION.SDK_INT
import android.provider.Settings
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Chronometer
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.ColorInt
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.itron.wh.androiddriver.service.aidl.IItronServiceApi
import com.itron.wh.androiddriver.service.aidl.IItronServiceCallback
import kotlinx.android.synthetic.main.activity_main.*
import org.java_websocket.WebSocket
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Double.parseDouble
import java.net.URI
import java.nio.ByteBuffer
import java.time.LocalDateTime
import android.widget.ProgressBar
import kotlinx.coroutines.*
import java.util.*
import okhttp3.*

import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.*
import android.net.Uri
import java.io.*
import android.content.res.AssetManager
import java.nio.file.Paths
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService


class MainActivity: AppCompatActivity(), WsServerListener,
    WsClientListner, IItronServiceCallback {

    val id: Int = 10
    val language: String = "kotlin"

    private var progressBar: ProgressBar? = null
    private val TAG = "WsServer"
    private val SERVER_IP = "127.0.0.1"
    private val SERVER_PORT = 5055
    var handler: Handler = Handler()
    var runnable: Runnable? = null
    var delay = 5000

    var counter: Int = 0

    private lateinit var wsServer: WsServer
    private var it: Intent? = null
    private var manualEntry: Boolean = false
    private var licenseUpdate: Boolean = false

    private val wsClient = WsClient(
        URI(
            "ws://$SERVER_IP:$SERVER_PORT"
        )
    )
    var commCommandsObj = CommunicationCommands()
    var clearCacheObj = ClearCache()

    private var cybleNumber: String = ""
    private lateinit var cybleNumbers: Array<String>
    private lateinit var requestedMeters: JSONArray
    private lateinit var meter: Chronometer
    private lateinit var meterResponse: TextView

    var client = OkHttpClient()
    var okHTTPRequest = OkHttpRequest(client)

    var okHTTPClient = RESTClient()

    val okHttpClient = OkHttpClient()
    val logging = RfLogs()
//    val sendDataToAWS = PutObjectInAWS()

    var otherDetails: Map<String, Any> = emptyMap()
    val rfMasterConenctionObj = RFMasterConnection(
        ::sendResponse,
        ::sendErrorResponse,
        ::setConnectionStatus,
        ::readCybles,
        ::sendBluetoothErrorResponse
    )


    @RequiresApi(Build.VERSION_CODES.N)
    val serverFormat = SimpleDateFormat("HH:mm:ss", Locale.getDefault())

    @RequiresApi(Build.VERSION_CODES.N)

    val itronConnection: ServiceConnection = object : ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            var timeCheck = isTimeAutomatic(applicationContext)
            if (timeCheck == true) {
                communicationWithRFMaster(service)

            } else {
                Toast.makeText(applicationContext, "Date/Time should be Automatic", Toast.LENGTH_LONG).show()
            }
        }

        override fun onServiceDisconnected(className: ComponentName) {
            rfMasterConenctionObj.myAidl.cancel("com.example.rfmaster")
            // myAidl.cancel("com.example.rfmaster")

        }
    }

    protected fun shouldAskPermissions(): Boolean {
        return if (SDK_INT >= Build.VERSION_CODES.R) {
            Environment.isExternalStorageManager()
        } else {
            val result: Int = ContextCompat.checkSelfPermission(this, READ_EXTERNAL_STORAGE)
            val result1: Int = ContextCompat.checkSelfPermission(this, WRITE_EXTERNAL_STORAGE)

            if(result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED ){
                return true
            }
            else
            {
                return false

            }
        }

    }

    @TargetApi(23)
    protected fun askPermissions() {
        val permissions = arrayOf(
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE"
        )
        val requestCode = 200
        requestPermissions(permissions, requestCode)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onResume() {
        handler.postDelayed(Runnable {
            handler.postDelayed(runnable!!, delay.toLong())
            logging.appendLog("RFMaster app isRunning: true",this, "RFLogs")

            //Toast.makeText(this@MainActivity, "This method will run every 10 seconds", Toast.LENGTH_SHORT).show()
        }.also { runnable = it }, delay.toLong())

        val delay = 900000 // 1000 milliseconds == 1 second
        handler.postDelayed(object : java.lang.Runnable {
            override fun run() {
                // Do your work here
                logging.checkFiles()
                handler.postDelayed(this, delay.toLong())
            }
        }, delay.toLong())

        super.onResume()
    }

    //@RequiresApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        val startIntent = Intent(this@MainActivity, ForegroundService::class.java)
        startService(startIntent)

        val versiontextView: TextView = findViewById(R.id.versionNumber) as TextView
        val connectionStatus: TextView = findViewById(R.id.connectionStatus) as TextView
        val moreInformation: TextView = findViewById(R.id.moreInfo) as TextView
        meterResponse = findViewById(R.id.meterResponse) as TextView
        loadLisence.setVisibility(View.GONE)
        clearCache.setVisibility(View.GONE)

        if (SDK_INT >= Build.VERSION_CODES.R) {
            if (!Environment.isExternalStorageManager()) {
                val uri = Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                startActivity(
                    Intent(
                        Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                        uri
                    )
                )
            }
        } else {
            shouldAskPermissions()
        }

        versiontextView.setOnLongClickListener(object : View.OnLongClickListener {
            override fun onLongClick(v: View?): Boolean {
                showLicenseUpdateButton()
                return false
            }
        })

        moreInformation.setOnClickListener {
            showDefaultDialog()
        }

        val cybleNumber: EditText = findViewById(R.id.cybleNumber) as EditText
        cybleNumber.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent): Boolean {
                rfMasterConenctionObj.myAidl.cancel("com.example.rfmaster")

                val DRAWABLE_LEFT = 0
                val DRAWABLE_TOP = 1
                val DRAWABLE_RIGHT = 2
                val DRAWABLE_BOTTOM = 3

                if (event.getAction() === MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= cybleNumber.getRight() - cybleNumber.getCompoundDrawables().get(DRAWABLE_RIGHT).getBounds().width()) {
                        // your action here
                        cybleNumber.setText("")
                        meterReading.setText("")
                        meterResponse.setText("")
                        cybleTimeStamp.setText("00:00:00")
                        meter.setText("00:00")
                        meter.stop()
                        return true
                    }
                }
                return false
            }
        })

        val reading: TextView = findViewById(R.id.meterReading) as TextView

        @ColorInt
        // val GREEN = -0xff0200
        var greenColor: Int = Color.parseColor("#037d50")

        reading.addBorder(greenColor, 15F)
        conenctServices()

        connectionStatus.setOnClickListener {
            checkBluetoothConenctionStatus()
        }

        meter = findViewById<Chronometer>(R.id.c_meter)

        submitButton.setOnClickListener {
            manualEntry = true
            meter.start()
            rfMasterConenctionObj.myAidl.cancel("com.example.rfmaster")
            @RequiresApi(Build.VERSION_CODES.N)

            if (cybleNumber.text.isEmpty()) {
                showAlert("Please enter a valid CybleNumber")
            }

            hideKeyboard()

            var isWorking = false
            if (!isWorking) {
                meter.setBase(SystemClock.elapsedRealtime());
                meter.start()
                isWorking = true
            } else {
                meter.stop()
                isWorking = false
            }

            meterResponse.text = ""
            cybleTimeStamp.text = "00:00:00"
            meterReading.text = ""
            getCybleNumberForManualEntry()
            conenctServices()
        }

        try {
            val pInfo: PackageInfo = getBaseContext().getApplicationContext().getPackageManager().getPackageInfo(getBaseContext().getApplicationContext().getPackageName(), 0)
            val version: String = pInfo.versionName
            versiontextView.text = "v." + version
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        wsServer = WsServer.init(SERVER_IP, SERVER_PORT)
        logging.appendLog("wsServer initialized on port: ${wsServer.port} \n",this, "RFLogs")

        wsServer.isReuseAddr = true
        try
        {
            logging.appendLog("Setting server listener \n",this, "RFLogs")
            wsServer.setListener(this)
            logging.appendLog("Set the Server listener successfully \n",this, "RFLogs")
        }
        catch (ex: Exception)
        {
            logging.appendLog("Failed to set server listener. Error - $ex \n",this, "RFLogs")
        }

        startServer()

    }


    fun isTimeAutomatic(c: Context): Boolean {
        return if (SDK_INT >= Build.VERSION_CODES.O) {
            Settings.Global.getInt(c.contentResolver, Settings.Global.AUTO_TIME, 0) === 1
        } else {
            Settings.System.getInt(c.contentResolver, Settings.System.AUTO_TIME, 0) == 1
        }
    }


    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("WrongConstant")
    private fun bindService() {

        it = Intent("com.itron.wh.androiddriver.service.intent.action.EXECUTE")
        it!!.setClassName(
            "com.itron.wh.androiddriver.service",
            "com.itron.wh.androiddriver.service.services.ItronDriverService"
        )
        try {
            getBaseContext().getApplicationContext()
                .bindService(it, itronConnection, BIND_AUTO_CREATE)
            logging.appendLog("ITRON Driver Services binding Succeeded \n",this, "RFLogs")

        } catch (e: java.lang.Exception) {
            println("Bind service failed");
            logging.appendLog("ITRON Driver Services binding failed \n",this, "RFLogs")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWsServerStatusChanged(isRunning: Boolean) {
        Log.d(TAG, "onWsServerStatusChanged is Running : $isRunning")
        runOnUiThread(Runnable {
            // txt_log.append("Server started on $SERVER_IP:$SERVER_PORT, is running: $isRunning \n\n")
        })
        logging.appendLog("Server Status isRunning: $isRunning \n",this, "RFLogs")
        logging.appendLog("Setting client listerner \n",this, "RFLogs")

        try {
            wsClient.setListener(this)
            logging.appendLog("Set the client listener successfully \n",this, "RFLogs")
        }
        catch (ex: Exception)
        {
            logging.appendLog("Failed to set client listeners. Error - $ex \n",this, "RFLogs")
        }
        if(!wsClient.isOpen)
        {
            try {
                wsClient.connect()
                logging.appendLog("Web socket Client connection successful \n",this, "RFLogs")
            }
            catch (ex: Exception)
            {
                logging.appendLog("Web socket Client connection error $ex \n",this, "RFLogs")
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessage(conn: WebSocket?, message: String?) {
        logging.appendLog("Reading request received from client: $message \n",this, "RFLogs")

        manualEntry = false
        val obj = JSONObject(message)
        val msgType: String
        val meters: JSONArray

        msgType = obj.getString("msgType")
        meters = obj.getJSONArray("meters")

        if (msgType == "readingRequest") {
            requestedMeters = meters

            getCybleNumber(meters)

            conenctServices()

        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessage(conn: WebSocket?, message: ByteBuffer?) {
        val v = String(message!!.array())
        logging.appendLog("Message received from client on Message Buffer: $v \n",this, "RFLogs")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWsServerMessageToClientSent(message: String) {
        var SPLogMessage: String =  message.substringAfterLast("\"meters\": [").substringBefore(']')
        logging.appendLog("$SPLogMessage \n",this, "SPLogs")
        logging.appendLog("$SPLogMessage \n",this, "AWS")
        logging.appendLog("Message sent to client successfully: $message \n", this, "RFLogs")
        runOnUiThread(Runnable {
            // txt_log.append("Message sent to client successfully: $message \n")
        })
    }


    override fun onWsNoConnections(message: String) {
        logging.appendLog("No Connection to Close $message \n",this, "RFLogs")
        runOnUiThread(Runnable {
            // txt_log.append("$message \n\n")
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWsClientConnectionClosed() {
        logging.appendLog("Client closed connection with the server \n", this, "RFLogs")
        logging.appendLog("Ws server isrunning: ${wsServer.isRunning}\n", this, "RFLogs")
        logging.appendLog("Ws client listener isOpen: ${wsClient.isOpen} \n",this, "RFLogs")
        runOnUiThread(Runnable {
            rfMasterConenctionObj.myAidl.cancel("com.example.rfmaster")
            // txt_log.append("Client closed connection with the server \n")
        })
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWsServerError(errorType: Int, errorMessage: String?) {
        logging.appendLog("Ws Server Error: errorType: $errorType >>> errorMessage: $errorMessage \n",this, "RFLogs")
        Log.d(TAG, "onWsServerError  : $errorType")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onWsServerConnChanged(connList: MutableList<String>?) {
        Log.d(TAG, "onWsServerConnChanged  ")
        var callingFunc = Thread.currentThread().stackTrace[3].methodName

        logging.appendLog("Called $callingFunc method \n",this, "RFLogs")
        logging.appendLog("Connection isReuseAddr: ${wsClient.isReuseAddr} \n",this, "RFLogs")
        logging.appendLog("Connection isTcpNoDelay: ${wsClient.isTcpNoDelay} \n",this, "RFLogs")
        logging.appendLog("Connection Lost Timeout: ${wsClient.connectionLostTimeout} \n",this, "RFLogs")

        if (connList != null) {
            for (con in connList)
                runOnUiThread(Runnable {
                    // txt_log.append("Ws Server Connection Changed: $con\n\n")
                })

            // commenting this for now since the client to communicate with is not the inbuilt client
            // coded inside this apk
            if (wsClient.isOpen){
                logging.appendLog("WsClient listener is Open : True\n",this, "RFLogs")
                logging.appendLog("WSServer listener is Running: ${wsServer.isRunning} \n ",this, "RFLogs")
            }
            else
            {
                logging.appendLog("Connection isClosed: ${wsClient.isClosed} \n ",this, "RFLogs")
                try {
                    wsClient.connect()
                    logging.appendLog("Web socket Client connection successful \n",this, "RFLogs")
                }
                catch (ex: Exception)
                {
                    logging.appendLog("Web socket Client connection error $ex \n",this, "RFLogs")
                }
            }
            //wsClient.send("Message from WS Client : ServerHandshake ")
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onClientMessage(message: String?) {
        runOnUiThread(Runnable {
            // txt_log.append("Message received from server: $message  \n")
        })
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun startServer() {
        val handler = Handler()
        logging.appendLog("Starting web socket server connection \n",this, "RFLogs")
        handler.postDelayed(Runnable {
            if(!wsServer.isRunning)
            {
                try{
                    wsServer.start()
                    logging.appendLog("Web socket server connection started successfully  \n",this, "RFLogs")
                }
                catch(e: Exception)
                {
                    logging.appendLog("Failed to start web socket server connection: $e \n",this, "RFLogs")
                }
            }
            else
            {
                logging.appendLog("Web socket server already running\n",this, "RFLogs")
            }

        }, 100)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun displayMeterReading(meterReading: String, readDateTime: String) {

        val reading: TextView = findViewById(R.id.meterReading) as TextView
        val readDate = readDateTime.substringBefore("T")
        val readTime = readDateTime.substringAfter("T")
        val timestamp: TextView = findViewById(R.id.cybleTimeStamp) as TextView

        try {
            val num = parseDouble(meterReading)
            var text = "%.3f".format(num)
            reading.text = text.toString()
            timestamp.text = readTime + " " + readDate
            meterResponse.text = "Success"

        } catch (e: NumberFormatException) {
            timestamp.text = readTime + " " + readDate
            reading.text = "Error!"
            meterResponse.text = meterReading
        }
    }


    private fun meterInformationCollected(requestedMetersArray: JSONArray, meterReading: String, serialNumber: String, readDateTime: String, meterData: Map<String, Any>) {
        var responseArray: Array<String> = arrayOf()
        var successMeterResponse: String = ""
        val readDate = readDateTime.substringBefore("T")
        val readTime = readDateTime.substringAfter("T")
        val alarmData = meterData.get("alarms")
        val pulseWeight = meterData.get("pulseWeight")
        val muiUnit = meterData.get("muiUnit")
        val backflow = meterData.get("backflowIndex")
        if (meterReading !== "") {
            for (i in 0 until requestedMetersArray.length()) {
                val meter = requestedMetersArray.getJSONObject(i)
                var meterId = meter.get("rfERTid")
                if (meterId == serialNumber) {
                    val pluginUniqueMeterId = meter.get("pluginUniqueId")
                    val rfERTid = meter.get("rfERTid")
                    try {
                        val num = parseDouble(meterReading)

                        successMeterResponse = successMeterResponse.plus(" \n" + "{\n" +
                                "\"pluginUniqueId\": \"$pluginUniqueMeterId\",\n" +
                                "\"meterId\": \"$meterId\",\n" +
                                "\"rfERTid\": \"$rfERTid\",\n" +
                                "\"readDate\": \"$readDate\",\n" +
                                "\"readTime\": \"$readTime\",\n" +
                                "\"reading\": \"$meterReading\",\n" +
                                "\"pulseWeight\": \"$pulseWeight\",\n" +
                                "\"backflow\":" + backflow + ",\n" +
                                alarmData +
                                "}")
                    } catch (e: NumberFormatException) {
                        val pluginUniqueId = meter.get("pluginUniqueId")
                        val rfERTid = meter.get("rfERTid")
                        val errorCode: String
                        if (meterReading.contains("did not respond")) {
                            errorCode = "220"
                        } else {
                            errorCode = "00"
                        }
                        successMeterResponse = successMeterResponse.plus("{\n" +
                                "\"pluginUniqueId\": \"$pluginUniqueMeterId\",\n" +
                                "\"meterId\": \"$meterId\",\n" +
                                "\"rfERTid\": \"$rfERTid\",\n" +
                                "\"readDate\": \"$readDate\",\n" +
                                "\"readTime\": \"$readTime\",\n" +
                                "\"errorNo\": \"$errorCode\",\n" +
                                "\"errorText\": \"$meterReading\"\n" +
                                "}")
                    }
                }
            }
            responseArray = append(successMeterResponse, responseArray.toMutableList())
            sendMessageToClient(responseArray)
        }
    }

    private fun getCybleNumber(requestedMetersArray: JSONArray) {
        cybleNumbers = arrayOf()
        val meter = requestedMetersArray.getJSONObject(0)
        val meterId: String = meter.get("meterId") as String
        cybleNumber = meterId
        for (i in 0 until requestedMetersArray.length()) {
            val meter = requestedMetersArray.getJSONObject(i)
            var meterId = meter.get("rfERTid") as String
            cybleNumbers = append(meterId, cybleNumbers.toMutableList())
        }
    }

    private fun getCybleNumberForManualEntry() {
        cybleNumbers = arrayOf()
        val textView: TextView = findViewById(R.id.cybleNumber) as TextView
        var cybleNumber = textView.text
        var string = cybleNumber.toString()
        cybleNumbers = append(string, cybleNumbers.toMutableList())

    }


    fun sendMessageToClient(responseArray: Array<String>) {
        wsServer.sendMessageToClient("{\n" +
                "\"msgType\": \"readingResponse\",\n" +
                "\"meterType\": \"remote\",\n" +
                "\"numMeters\": ${responseArray.size.toInt()},\n" +
                "\"meters\": ${Arrays.toString(responseArray)} \n" +
                "}")

    }


    fun append(element: String, list: MutableList<String>): Array<String> {
        list.add(element)
        return list.toTypedArray()
    }

    override fun onStatusUpdated(message: String?) {
        print(message)
        Log.d("msg", "Message")
    }

    override fun asBinder(): IBinder {
        TODO("Not yet implemented")
    }


    fun communicationWithRFMaster(service: IBinder) {
        rfMasterConenctionObj.myAidl = IItronServiceApi.Stub.asInterface(service)
        checkBluetoothConenction()
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun sendResponse(muiValue: String, meterData: Map<String, Any>, serialNumber: String, readDateTime: String) {
        otherDetails = meterData

        if (manualEntry == true) {
            meter.stop()

            displayMeterReading(muiValue, readDateTime)
        } else if (licenseUpdate == true) {
            this.runOnUiThread(java.lang.Runnable {
                AlertDialog.Builder(this)
                    .setTitle("Success")
                    .setMessage(muiValue)
                    .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                    .show()
            })
            loadLisence.setVisibility(View.GONE)
        } else {
            meterInformationCollected(requestedMeters, muiValue, serialNumber, readDateTime, meterData)
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun sendErrorResponse(errorMessage: String, errorCode: String, serialNumber: String) {
        if (errorCode == "103") {
            runOnUiThread(Runnable {
                connectionStatus.setTextColor(Color.RED)
                connectionStatus.text = "RFMaster Status:Not Connected"
            })
        }
        if (manualEntry == true) {
            meter.stop()
            meterResponse.text = errorMessage
            meterReading.text = "Error!"
        } else if (licenseUpdate == true) {
            this.runOnUiThread(java.lang.Runnable {
                AlertDialog.Builder(this)
                    .setTitle("Something Went Wrong")
                    .setMessage(errorMessage)
                    .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                        dialog.cancel()
                    })
                    .show()
            })

            loadLisence.setVisibility(View.GONE)
        } else {
            if (serialNumber == "") {
                sendBluetoothErrorResponse(errorMessage, errorCode)
            } else {
                meterInformationCollected(requestedMeters, errorMessage, serialNumber, LocalDateTime.now().withNano(0).toString(), emptyMap())
            }
        }

    }


    @RequiresApi(Build.VERSION_CODES.O)
    private fun sendBluetoothErrorResponse(errorMessage: String, errorCode: String) {
        if (manualEntry == true) {
            meter.stop()
            meterResponse.text = errorMessage
            meterReading.text = "Error!"
            connectionStatus.setTextColor(Color.RED)
            connectionStatus.text = "RFMaster Status: Not Connected"

        } else {
            for (i in 0 until requestedMeters.length()) {
                val meter = requestedMeters.getJSONObject(i)
                val pluginUniqueId = meter.get("pluginUniqueId")
                val meterId = meter.get("meterId")
                val rfERTid = meter.get("rfERTid")
                val readDateTime: String = LocalDateTime.now().withNano(0).toString()
                val readDate = readDateTime.substringBefore("T")
                val readTime = readDateTime.substringAfter("T")

                wsServer.sendMessageToClient(
                    "{\n" +
                            "\"msgType\": \"readingResponse\",\n" +
                            "\"meterType\": \"remote\",\n" +
                            "\"numMeters\": 1,\n" +
                            "\"meters\": [\n" +
                            "{\n" +
                            "\"pluginUniqueId\": \"$pluginUniqueId\",\n" +
                            "\"meterId\": \"$meterId\",\n" +
                            "\"rfERTid\": \"$rfERTid\",\n" +
                            "\"readDate\": \"$readDate\",\n" +
                            "\"readTime\": \"$readTime\",\n" +
                            "\"errorNo\": \"$errorCode\",\n" +
                            "\"errorText\": \"$errorMessage\"\n" +
                            " \n" +
                            "}]\n" +
                            "}"
                )

            }

        }
    }


    fun conenctServices() {
        if (it !== null) {
            getBaseContext().getApplicationContext().unbindService(itronConnection)
            bindService()
        } else {
            bindService()
        }
    }


    fun checkBluetoothConenction() {
        val allConnections = commCommandsObj.getAllConnections()
        rfMasterConenctionObj.myAidl.send("com.example.rfmaster", allConnections.toString(), rfMasterConenctionObj.myAidlCallBackForConnections)
    }

    fun checkBluetoothConenctionStatus() {
        val allConnections = commCommandsObj.getAllConnections()
        rfMasterConenctionObj.myAidl.send("com.example.rfmaster", allConnections.toString(), rfMasterConenctionObj.myAidlCallBackForConnectionCheck)
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }


    fun setConnectionStatus(status: String) {
        runOnUiThread(Runnable {
            if (status == "RFMaster Status: Not Connected") {
                connectionStatus.setTextColor(Color.RED)
            } else {
                connectionStatus.setTextColor(Color.GREEN)
            }

            connectionStatus.text = status
        })

    }


    fun readCybles() {
        runOnUiThread(Runnable {
            connectionStatus.setTextColor(Color.GREEN)
            connectionStatus.text = "RFMaster Status:Connected"
        })

        if (this::cybleNumbers.isInitialized) {
            if (cybleNumbers.size > 8) {
                var list = cybleNumbers.toList()
                var ab = list.chunked(8)
                for (i in 0 until ab.size) {
                    val req = commCommandsObj.readCybles(ab[i].toTypedArray())
                    rfMasterConenctionObj.myAidl.send("com.example.rfmaster", req.toString(), rfMasterConenctionObj.myAidlCallBackForCybleReading)
                }
            } else {
                val req = commCommandsObj.readCybles(cybleNumbers)
                rfMasterConenctionObj.myAidl.send("com.example.rfmaster", req.toString(), rfMasterConenctionObj.myAidlCallBackForCybleReading)
            }
        }

    }


    private fun showLicenseUpdateButton() {
        progressBar = findViewById<ProgressBar>(R.id.Progress_Bar) as ProgressBar

        loadLisence.setVisibility(View.VISIBLE);
        loadLisence.setOnClickListener {
            progressBar!!.visibility = View.VISIBLE
            okHTTPClient.getAccessToken(){result ->
                if (result !== "")
                {
                    okHTTPClient.loadLicense(result){response ->
                        if(response == "Success")
                        {
                            licenseUpdate = true
                            val allConnections = commCommandsObj.getAllConnections()
                            try {
                                val req = commCommandsObj.updatelicense()
                                var result = rfMasterConenctionObj.myAidl.send("com.example.rfmaster", req.toString(), rfMasterConenctionObj.myAidlCallBackForLicenseUpdate)
                                progressBar!!.visibility = View.INVISIBLE
                            } catch (e: java.lang.Exception) {
                                progressBar!!.visibility = View.INVISIBLE
                                print(e)
                            }
                        }
                        else
                        {
                            progressBar!!.visibility = View.INVISIBLE
                            showAlert("Directory not found")
                        }
                    }
                }
            }

        }
    }





    private fun showClearCacheButton() {
        clearCache.setVisibility(View.VISIBLE);
        clearCache.setOnClickListener {
            try {
                clearCacheObj.deleteCache(this@MainActivity)
                clearCache.setVisibility(View.GONE)
                Toast.makeText(applicationContext, "Cache has been cleared successfully", Toast.LENGTH_LONG).show()
            } catch (e: java.lang.Exception) {
                print(e)
            }

        }
    }


    private fun showDefaultDialog() {
        val alertDialog = AlertDialog.Builder(this)
        if (!otherDetails.isEmpty()) {
            alertDialog.apply {
                setTitle("Meter Information")
                val infoString = otherDetails.get("alarms").toString()
                val backFlow = otherDetails.get("backflowIndex").toString()
                val pulseWeight = otherDetails.get("pulseWeight").toString()
                val infoStringFormatted: String = infoString.replace(",", System.getProperty("line.separator"))
                setMessage(infoStringFormatted + "\n" + "BackFlow:" + backFlow + "\n" + "pulseWeight:" + pulseWeight)

                setPositiveButton("Close") { _, _ ->
                }

            }.create().show()
        } else {
            alertDialog.apply {
                setTitle("Meter Information")
                setMessage("No information")

                setPositiveButton("Close") { _, _ ->
                }

            }.create().show()
        }
    }


    fun showAlert(message: String){
        val dialogBuilder = AlertDialog.Builder(this)
        // set message of alert dialog
        dialogBuilder.setMessage(message)
            // if the dialog is cancelable
            .setCancelable(false)

            // negative button text and action
            .setNegativeButton("OK", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        runOnUiThread(Runnable {
            // create dialog box
            val alert = dialogBuilder.create()
            // set title for alert dialog box
            alert.setTitle("Alert")
            // show alert dialog
            alert.show()
        })
    }

}
