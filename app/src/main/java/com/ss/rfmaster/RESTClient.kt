package com.ss.rfmaster

import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.os.Build
import android.os.Environment
import androidx.annotation.RequiresApi
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.File
import java.io.IOException
import okhttp3.RequestBody
import java.io.BufferedReader
import java.io.FileReader
import java.lang.StringBuilder
import okhttp3.MultipartBody
import java.util.*


class RESTClient {
    val okHttpClient = OkHttpClient()
    private val accesstokenURL = "https://accounts.accesscontrol.windows.net/ac1df4be-765d-4ffc-8dfd-21d263d4a049/tokens/OAuth/2"
    private val siteBaseURL = "https://servicestrm.sharepoint.com/sites/Intranet/ew/"
    private val clientId = "67f381c5-798e-408a-973d-f1b0c1b11db2@ac1df4be-765d-4ffc-8dfd-21d263d4a049"
    private val clientSecret = "pHq8Q~YdyE_85YBDmz7FBOI5v1_8MaPiIX7DycCB"

    private val fileName = "ItronLicenseGenericLisense.lic"
    private var accessToken = ""
    val writeFileObj = WriteFile()

    fun getAccessToken(myCallback: (result: String) -> Unit)
    {
        val requestBody = RequestBody.create(MediaType.parse("application/x-www-form-urlencoded"), "grant_type=client_credentials&client_id="+clientId+"&resource=00000003-0000-0ff1-ce00-000000000000/servicestrm.sharepoint.com@ac1df4be-765d-4ffc-8dfd-21d263d4a049&client_secret="+clientSecret)

        val request = Request.Builder()
            .method(
                "POST",requestBody
            )
            .url(accesstokenURL)
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                val responseData = response.body()?.string()
                try {
                    var jsonResponse = JSONObject(responseData)
                    accessToken = jsonResponse.getString("access_token")
                    myCallback.invoke(accessToken)
                    println("Request Successful!!")

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

        })
    }


    fun loadLicense(accessToken: String, result: (result: String) -> Unit)
    {
        val licenseRequest = Request.Builder()
            .method("GET",
                null
            )
            .addHeader("Authorization", "Bearer $accessToken")
            .addHeader("ApiKey","500258")
            .addHeader("Connection","keep-allive")
            .addHeader("Content-Type", "application/json;odata=verbose")
            .url(siteBaseURL + "_api/web/getfilebyurl('LINK%20TRAINING%20MATERIAL/RFMaster/$fileName')/$"+"value")
            .build()

        okHttpClient.newCall(licenseRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(call: Call, response: Response) {
                try{
                    println("Request Successful!!")
                    writeFileObj.writeFile(response) { response ->
                        if(response == "Success")
                        {
                            result.invoke("Success")
                        }
                        else
                        {
                            result.invoke(response)
                        }
                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        })
    }

    fun uploadFile(accessToken: String,file: File, result: (result: String) -> Unit)
    {
        val mediaType = MediaType.parse("Text/Plain")
        val body = RequestBody.create(mediaType, file)
        val cal = Calendar.getInstance()
        val resultdate = Date(cal.getTimeInMillis())
        val sdf = SimpleDateFormat("YYY-MM-dd HH-mm")
        var dateString = sdf.format(resultdate).replace(" ","-")

        val fileUploadRequest = Request.Builder()
            .method("POST",
                body
            )

            .addHeader("Authorization", "Bearer $accessToken")
            .addHeader("ApiKey","500258")
            .addHeader("Connection","keep-alive")
            .url(siteBaseURL+"_api/web/GetFolderByServerRelativeUrl('LINK Training Material/RFMaster/Log')/Files/Add(url='${file.name.replace(".txt","$dateString.txt")}',overwrite='true')")

            .build()

        okHttpClient.newCall(fileUploadRequest).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onResponse(call: Call, response: Response) {
                try{
                    println("Request Successful!!")
                    file.delete()
//                    writeFileObj.writeFile(response) { response ->
//                        if(response == "Success")
//                        {
//                            result.invoke("Success")
//                        }
//                        else
//                        {
//                            result.invoke(response)
//                        }
//                    }
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }
        })
    }

}