package com.itron.wh.androiddriver.service.aidl;

import com.itron.wh.androiddriver.service.aidl.IItronServiceCallback;

interface IItronServiceApi {

	 int send(String userApplicationName, String command, IItronServiceCallback callback);
	 int cancel(String userApplicationName);
}

